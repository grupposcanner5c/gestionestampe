﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using SoftwarePalestra;

namespace GestioneStampe
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DBQueries db = new DBQueries("localhost", "root", "", "mypalestra");
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonAnagrafica_Click(object sender, RoutedEventArgs e)
        {
            printAnagrafica.Visibility = Visibility.Visible;
            printCorso.Visibility = Visibility.Hidden;
            printIscrizione.Visibility = Visibility.Hidden;            
        }

        private void ButtonCorso_Click(object sender, RoutedEventArgs e)
        {
            printAnagrafica.Visibility = Visibility.Hidden;
            printCorso.Visibility = Visibility.Visible;
            printIscrizione.Visibility = Visibility.Hidden;

            List<Corso> corso = db.GetCorso();

            for (int i = 0; i < corso.Count; i++)
            {
                ListBoxCorsi.Items.Add(corso[i].Nome);
            }    
              
        }

        private void ButtonIscrizione_Click(object sender, RoutedEventArgs e)
        {
            printAnagrafica.Visibility = Visibility.Hidden;
            printCorso.Visibility = Visibility.Hidden;
            printIscrizione.Visibility = Visibility.Visible;
        }

        private void ButtonStampa_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Visual gridStampa = null;
                if (printAnagrafica.IsVisible)
                {
                    gridStampa = printAnagrafica;
                }
                if (printCorso.IsVisible)
                {
                    gridStampa = printCorso;
                }
                if (printIscrizione.IsVisible)
                {
                    gridStampa = printIscrizione;
                }

                if (gridStampa != null)
                {
                    this.IsEnabled = false;
                    PrintDialog printDialog = new PrintDialog();
                    if (printDialog.ShowDialog() == true)
                    {
                        printDialog.PrintVisual(gridStampa, "stampa");
                    }
                }
            }
            finally
            {
                this.IsEnabled = true;
            }
        }

        private void TextBoxCerca_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TextBoxCerca.Text == "Nome da cercare")
            {
                TextBoxCerca.Text = "";
            }
        }

        private void TextBoxCerca_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBoxCerca.Text == "")
            {
                TextBoxCerca.Text = "Nome da cercare";
            }
        }

        private void ButtonCerca_Click(object sender, RoutedEventArgs e)
        {
            string query = "SELECT ID, Nome, Cognome FROM `cliente` WHERE cliente.Nome LIKE '" + TextBoxCerca.Text + "%' OR cliente.Cognome LIKE '" + TextBoxCerca.Text + "%'";
            //return List<User>

            List<User> users = new List<User>();
            users.Add(new User() { Id = 1, Nome = "John Doe", Cognome = "John Doe", dataNascita = new DateTime(1991, 9, 2), dataRegistrazione = new DateTime(2000, 1, 1) });
            users.Add(new User() { Id = 2, Nome = "Jane Doe", Cognome = "John Doe", dataNascita = new DateTime(1900, 11, 9), dataRegistrazione = new DateTime(2000, 1, 1) });
            users.Add(new User() { Id = 3, Nome = "Sammy Doe", Cognome = "John Doe", dataNascita = new DateTime(1990, 5, 1), dataRegistrazione = new DateTime(2000, 1, 1) });

            DataGridViewAnagrafica.ItemsSource = users;
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            //ottenere id della riga selezionata
            object item = DataGridViewAnagrafica.SelectedItem;
            string ID = (DataGridViewAnagrafica.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text;

            WindowSpecificUser w = new WindowSpecificUser(ID);
            w.Show();
        }

        private void ButtonCalcolaCorso_Click(object sender, RoutedEventArgs e)
        {
            string query = "numero totale di ingressi di un corso in un dato periodo";
            //return int
            //int num = db.getNumeroIngressi();
            //LabelTotIngressi.Content = "Totale ingressi: " + num;
        }

        private void ButtonCalcolaIscrizione_Click(object sender, RoutedEventArgs e)
        {
            string query = "Dato in input: data iniziale e finale; Mi serve: nome, cognome, data rinnovo, numero ingressi rinnovati, costo, se era prima iscrizione";
            //return List<Iscrizione>
            /*
            List<Iscrizione> Iscrizione = db.getIscrizione();
            DataGridViewIscrizione.ItemsSource = Iscrizione;
            */
        }
    }

    public class User
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime dataNascita { get; set; }
        public DateTime dataRegistrazione { get; set; }
    }

    public class Iscrizione
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public DateTime DataRinnovo { get; set; }
        public int NIngressiRinnovati { get; set; }
        public int Costo { get; set; }
        public bool PrimaIscrizione { get; set; }
    }
}
