﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwarePalestra
{
    class DBQueries
    {
        private string server;
        private string userid;
        private string password;
        private string database;
        private string mySQLConnectionString;

        public DBQueries(string server, string userid, string password, string database)
        {
            //localhost
            this.server = server;
            //root
            this.userid = userid;
            //mypalestra
            this.database = database;
            //password
            this.password = password;
            mySQLConnectionString = $"server={server};userid={userid};database={database};";

        }


        public List<Corso> GetCorso()
        {
            List<Corso> corsi = new List<Corso>();
            int id;
            string nome;
            string query = "SELECT * FROM corso";
            using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
            {
                mysqlconnection.Open();
                using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 300;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                    //solo se è una query che riporta qualcosa
                    MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                    mysqldatareader.Read();

                    //result = mysqldatareader.GetString(1);
                    do
                    {
                        id = mysqldatareader.GetInt32("ID");
                        nome = mysqldatareader.GetString("Nome");
                        corsi.Add(new Corso(id, nome));

                    } while (mysqldatareader.NextResult());
                }
            }

            return corsi;
        }

        public void CreateCorso(int id, string nome)
        {
            string query = $"INSERT INTO corso (ID,Nome) VALUES ({id},'{nome}');";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                    mysqlconnection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void UpdateInt(string tabella, int colonna, int valore)
        {
            //string result;
            string query = "UPDATE " + tabella + " SET " + colonna + "=" + valore + ";";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta qualcosa
                        /*
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetString(colonna);
                        */
                    }
                }
            }
            finally
            {

            }

        }

        public void UpdateString(string tabella, int colonna, string valore)
        {
            //string result;
            string query = "UPDATE " + tabella + " SET " + colonna + "=" + valore + ";";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta qualcosa
                        /*
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetString(colonna);
                        */
                    }
                }
            }
            finally
            {

            }

        }

        public void Delete(string tabella, string condizione)
        {
            //string result;
            string query = "DELETE FROM " + tabella + " WHERE " + condizione + ";";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta qualcosa
                        /*
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetString(colonna);
                        */

                    }
                }
            }
            finally
            {

            }

        }

        public DateTime GetDate(string campo, string tabella, int colonna)
        {
            DateTime result;
            string query = "SELECT" + campo + " FROM " + tabella + ";";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta qualcosa 

                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetDateTime(colonna);


                    }
                }
            }
            finally
            {

            }
            return result;
        }
        public void CreateTessera(Boolean tipoTessera, int numeroEntrate)
        {
            //true nuova - false rinnovo
            string query = $"INSERT INTO tessera (tipoTessera,NEntrate) VALUES ({tipoTessera},{numeroEntrate});";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {

            }
        }
        public string GetEntrate(int id)
        {
            string result = null;
            //true nuova - false rinnovo
            string query = $"SELECT NEntrate FROM tessera WHERE id = {id}";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta 
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetInt64("NEntrate").ToString();
                    }
                }
            }
            finally
            {

            }
            return result;
        }

        public string GetNome(int id)
        {
            string result;
            string query = $"SELECT Nome FROM cliente WHERE id = {id}";
            try
            {
                using (var mysqlconnection = new MySqlConnection(mySQLConnectionString))
                {
                    mysqlconnection.Open();
                    using (MySqlCommand cmd = mysqlconnection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 300; //In ms
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        //solo se è una query che riporta 
                        MySqlDataReader mysqldatareader = cmd.ExecuteReader();
                        mysqldatareader.Read();
                        result = mysqldatareader.GetString("Nome");
                    }
                }
            }
            finally
            {

            }
            return result;
        }
    }
    class Corso
    {
        private int id;
        private string nome;

        public Corso(int ID, string nome)
        {
            id = ID;
            this.nome = nome;
        }
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
    }

    class Cliente
    {
        private int id;
        private int tesseraId;
        private string nome;
        private string cognome;
        private string email;
        private int telefono;
        private string indirizzo;
        private int ncivico;
        private string citta;
        private DateTime dataregistrazione;
        private DateTime dataNascita;

        public Cliente(int id, int tesseraId, string nome, string cognome, string email, int telefono, string indirizzo, int ncivico, string citta, DateTime dataregistrazione, DateTime dataNascita)
        {
            this.id = id;
            this.tesseraId = tesseraId;
            this.nome = nome;
            this.cognome = cognome;
            this.email = email;
            this.telefono = telefono;
            this.indirizzo = indirizzo;
            this.ncivico = ncivico;
            this.citta = citta;
            this.dataregistrazione = dataregistrazione;
            this.dataNascita = dataNascita;
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int tesseraID
        {
            get { return tesseraId; }
            set { tesseraId = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        public string Cognome
        {
            get { return Cognome; }
            set { Cognome = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public int Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }
        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public int NCivico
        {
            get { return ncivico; }
            set { ncivico = value; }
        }

        public string Citta
        {
            get { return citta; }
            set { citta = value; }
        }

        public DateTime DataRegistrazione
        {
            get { return dataregistrazione; }
            set { dataregistrazione = value; }
        }

        public DateTime DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }
    }
}
