﻿using SoftwarePalestra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GestioneStampe
{
    /// <summary>
    /// Logica di interazione per WindowSpecificUser.xaml
    /// </summary>
    public partial class WindowSpecificUser : Window
    {
        private DBQueries db = new DBQueries("localhost", "root", "", "mypalestra");
        public WindowSpecificUser(string id)
        {
            InitializeComponent();

            //string id = IDCliente.Content.ToString();
            Thickness margin;
            
            //query per impostare la label dei dati anagrafici
            queryAnagraficaCliente(id);

            //setto altezza del dataGridRinnovi in base alle righe risultate dalla query
            int heightRinnovi = queryRinnovi(id);
            dataGridRinnovi.Height = 20 * heightRinnovi + 25;


            //modifico la posizione dal top in base all'altezza dei dataGrid precedenti
            margin = labelIngressiTotali.Margin;
            margin.Top += dataGridRinnovi.Height - 20;
            labelIngressiTotali.Margin = margin;

            margin = dataGridIngressiCorso.Margin;
            margin.Top += dataGridRinnovi.Height - 20;
            dataGridIngressiCorso.Margin = margin;

            //setto altezza del dataGridRinnovi in base alle righe risultate dalla query
            int heightIngressiCorso = queryIngressiCorso(id);
            dataGridIngressiCorso.Height = 20 * heightIngressiCorso + 25;

            //modifico la posizione dal top in base all'altezza dei dataGrid precedenti
            margin = labelResocontoEntrate.Margin;
            margin.Top += dataGridRinnovi.Height + dataGridIngressiCorso.Height - 40;
            labelResocontoEntrate.Margin = margin;

            margin = dataGridEntrate.Margin;
            margin.Top += dataGridRinnovi.Height + dataGridIngressiCorso.Height - 40;
            dataGridEntrate.Margin = margin;

            //setto altezza del dataGridRinnovi in base alle righe risultate dalla query
            int heightEntrateSingole = queryEntrateSingole(id);
            dataGridEntrate.Height = 20 * heightEntrateSingole + 25;

            //altezza bottone stampa
            margin = ButtonStampaSpecificUser.Margin;
            margin.Top += dataGridRinnovi.Height + dataGridIngressiCorso.Height + dataGridEntrate.Height - 60;
            ButtonStampaSpecificUser.Margin = margin;

            //label nascosta finale per mettere del margine alla fine
            margin = labelFine.Margin;
            margin.Top += dataGridRinnovi.Height + dataGridIngressiCorso.Height + dataGridEntrate.Height - 60;
            labelFine.Margin = margin;
        }
        
        private void queryAnagraficaCliente(string id)
        {
            Cliente cliente = db.getCliente(id);
            labelDatiCliente.Content = cliente.Nome + cliente.Cognome + "  -  " + cliente.DataNascita + "  -  " + cliente.Citta + cliente.Indirizzo + cliente.NCivico + "  -  " + cliente.Telefono + "  -  " + cliente.Email;
            labelDataIscrizione.Content = "Data iscrizione" + cliente.DataRegistrazione;
        }
        
        private int queryRinnovi(string id)
        {
            List<Rinnovi> rinnovi = db.getRinnovi(id);
            dataGridRinnovi.ItemsSource = rinnovi;

            return rinnovi.Count;
        }

        private int queryEntrateSingole(string id)
        {
            List<IngressiCorso> ingressiCorso = db.getIngressiTotaliPerCorso(id);
            dataGridIngressiCorso.ItemsSource = ingressiCorso;

            return ingressiCorso.Count;
        }

        private int queryIngressiCorso(string id)
        {
            List<EntrateSingole> entrateSingole = db.getSingoleEntrate(id);
            dataGridEntrate.ItemsSource = entrateSingole;

            return entrateSingole.Count;
        }

        private void ButtonStampaSpecificUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Visual gridStampa = gridStampaSpecificUser;
                if (gridStampa != null)
                {
                    this.IsEnabled = false;
                    PrintDialog printDialog = new PrintDialog();
                    if (printDialog.ShowDialog() == true)
                    {
                        printDialog.PrintVisual(gridStampa, "stampa");
                    }
                }
            }
            finally
            {
                this.IsEnabled = true;
            }
        }
    }

    public class Rinnovi
    {
        public DateTime dataRinnovo { get; set; }
        public int nIngressi { get; set; }
    }

    public class IngressiCorso
    { 
        public string corso { get; set; }
        public int nIngressi { get; set; }
    }

    public class EntrateSingole
    {
        public DateTime dataRinnovo { get; set; }
        public string corso { get; set; }
    }
}
